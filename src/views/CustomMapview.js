import React from 'react';
import {View, Text, StyleSheet, Dimensions, Image} from 'react-native';
import MapView, {Marker, Polyline} from 'react-native-maps';
import {computeDistanceBetween} from '@goparrot/react-native-geometry-utils';

import data_run_1 from '../../Json/data_run_1.json';
import data_run_2 from '../../Json/data_run_2.json';
import data_run_3 from '../../Json/data_run_3.json';
import data_run_4 from '../../Json/data_run_4.json';
import data_run_5 from '../../Json/data_run_5.json';

const {width, height} = Dimensions.get('window');

class CustomMapview extends React.Component {
  constructor(props) {
    super(props);
    var fileName;
    switch (props.route.params.item) {
      case 1:
        fileName = data_run_1;
        break;
      case 2:
        fileName = data_run_2;
        break;
      case 3:
        fileName = data_run_3;
        break;
      case 4:
        fileName = data_run_4;
        break;
      case 5:
        fileName = data_run_5;
        break;
      default:
        fileName = data_run_1;
        break;
    }

    const ASPECT_RATIO = width / height;
    const LATITUDE = fileName[0].LATITUDE_DEG;
    const LONGITUDE = fileName[0].LONGITUDE_DEG;
    const LATITUDE_DELTA = 0;
    const LONGITUDE_DELTA = LATITUDE_DELTA * ASPECT_RATIO;

    const END_LATITUDE = fileName[fileName.length - 1].LATITUDE_DEG;
    const END_LONGITUDE = fileName[fileName.length - 1].LONGITUDE_DEG;
    const END_LONGITUDE_DELTA = LATITUDE_DELTA * ASPECT_RATIO;

    this.state = {
      region: {
        latitude: LATITUDE,
        longitude: LONGITUDE,
        latitudeDelta: LATITUDE_DELTA,
        longitudeDelta: LONGITUDE_DELTA,
      },
      endRegion: {
        latitude: END_LATITUDE,
        longitude: END_LONGITUDE,
        latitudeDelta: LATITUDE_DELTA,
        longitudeDelta: END_LONGITUDE_DELTA,
      },
      fileName,
    };
  }

  state = {
    events: [],
    distance: 0.0,
    time: 0.0,
  };

  async componentDidMount() {
    const {fileName} = this.state;
    const response = await this.calcPathLength(fileName);
    this.setState({
      distance: response,
      time: fileName[fileName.length - 1].TIME - fileName[0].TIME,
    });
  }

  async calcPathLength(path) {
    var total = 0;
    for (var i = 0; i < path.length - 1; i++) {
      var pos1 = {
        latitude: path[i].LATITUDE_DEG,
        longitude: path[i].LONGITUDE_DEG,
      };
      var pos2 = {
        latitude: path[i + 1].LATITUDE_DEG,
        longitude: path[i + 1].LONGITUDE_DEG,
      };
      total += await computeDistanceBetween(pos1, pos2);
    }
    return total;
  }

  render() {
    const {distance, time, region, endRegion, fileName} = this.state;
    var travelTime =
      time !== undefined
        ? `${Math.floor(time / 60)}:${Math.floor(time % 60)}:00`
        : 'loading...';
    return (
      <View style={styles.container}>
        <MapView
          provider="google"
          mapType="satellite"
          style={styles.map}
          scrollEnabled
          zoomEnabled
          pitchEnabled
          rotateEnabled
          showsUserLocation
          showsMyLocationButton
          toolbarEnabled
          initialRegion={region}>
          <Polyline
            coordinates={[
              ...fileName.map(value => {
                return {
                  latitude: value.LATITUDE_DEG,
                  longitude: value.LONGITUDE_DEG,
                };
              }),
            ]}
            strokeColor="#c93e3e"
            strokeWidth={3}
          />
          <Marker title="Start" coordinate={region}>
            <Image
              source={require('../images/start.png')}
              style={styles.imageSize}
            />
          </Marker>

          <Marker title="End" coordinate={endRegion}>
            <Image
              source={require('../images/finish.png')}
              style={styles.imageSize}
            />
          </Marker>
        </MapView>

        <View style={styles.internalView}>
          <View style={[styles.textView, styles.textLeftView]}>
            <Text style={[styles.text]}>Time</Text>
            <Text style={[styles.text, styles.title]}>{travelTime}</Text>
          </View>

          <View style={styles.textView}>
            <Text style={[styles.text]}>Distance</Text>
            <Text style={[styles.text, styles.title]}>
              {distance !== undefined ? distance.toFixed(2) : 'loading...'}
            </Text>
          </View>

          <View style={styles.lineView} />

          <View style={[styles.textView, styles.textLeftView]}>
            <Text style={[styles.text]}>AVG Pace</Text>
            <Text style={[styles.text, styles.title]}>
              {distance !== undefined
                ? (distance / time).toFixed(2)
                : 'loading...'}
            </Text>
          </View>

          <View style={styles.textView}>
            <Text style={[styles.text]}>Altitude</Text>
            <Text style={[styles.text, styles.title]}>
              {Math.floor(
                Math.max.apply(
                  Math,
                  fileName.map(o => o.ALTITUDE),
                ),
              )}
            </Text>
          </View>
        </View>
      </View>
    );
  }
}

export default CustomMapview;

const styles = StyleSheet.create({
  container: {
    position: 'absolute',
    top: 0,
    left: 0,
    right: 0,
    bottom: 0,
    justifyContent: 'flex-end',
    alignItems: 'center',
  },
  map: {
    position: 'absolute',
    top: 0,
    left: 0,
    right: 0,
    bottom: 0,
    flex: 1,
    height: '50%',
    width: '100%',
  },
  internalView: {
    backgroundColor: 'white',
    height: '50%',
    width: '100%',
    flexDirection: 'row',
    flexWrap: 'wrap',
    justifyContent: 'space-between',
    paddingTop: 10,
  },
  textView: {
    width: '50%',
    alignItems: 'center',
  },
  lineView: {
    height: 1,
    width: '100%',
    backgroundColor: '#ccc',
    marginVertical: 10,
  },
  textLeftView: {
    borderRightWidth: 1,
    borderRightColor: '#ccc',
  },
  text: {
    fontSize: 14,
    paddingHorizontal: 10,
  },
  title: {
    fontWeight: 'bold',
    fontSize: 22,
  },
  imageSize: {
    height: 50,
    width: 50,
  },
});
