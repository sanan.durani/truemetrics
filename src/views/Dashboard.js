import React from 'react';
import {View, Text, FlatList, TouchableOpacity, StyleSheet} from 'react-native';

class Dashboard extends React.Component {
  state = {
    mapButtons: [1, 2, 3, 4, 5],
  };

  render() {
    const {navigation} = this.props;
    const {mapButtons} = this.state;

    return (
      <View>
        <FlatList
          data={mapButtons}
          numColumns={2}
          keyExtractor={(_, index) => index}
          renderItem={({item}) => {
            return (
              <TouchableOpacity
                style={styles.button}
                onPress={() => navigation.navigate('Mapview', {item})}>
                <View style={styles.mainView}>
                  <Text style={styles.text}>{`Path ${item}`}</Text>
                </View>
              </TouchableOpacity>
            );
          }}
        />
      </View>
    );
  }
}

export default Dashboard;

const styles = StyleSheet.create({
  button: {
    width: '50%',
    height: 200,
    justifyContent: 'center',
    alignItems: 'center',
  },
  mainView: {
    height: '90%',
    width: '90%',
    justifyContent: 'center',
    alignItems: 'center',
    borderWidth: 1,
    borderColor: '#ccc',
    borderRadius: 5,
    backgroundColor: 'white',
  },
  text: {
    fontSize: 18,
    textTransform: 'uppercase',
  },
});
