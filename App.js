import React, {Component} from 'react';
import {StyleSheet, ActivityIndicator, Image, View, Text} from 'react-native';

import {NavigationContainer} from '@react-navigation/native';
import {createStackNavigator} from '@react-navigation/stack';

import Dashboard from './src/views/Dashboard';
import CustomMapview from './src/views/CustomMapview';

const Stack = createStackNavigator();

export default class App extends Component {
  constructor(props) {
    super(props);
  }

  componentDidMount() {}

  state = {};

  render() {
    return (
      <NavigationContainer>
        <Stack.Navigator>
          <Stack.Screen name="Dashboard" component={Dashboard} />
          <Stack.Screen name="Mapview" component={CustomMapview} />
        </Stack.Navigator>
      </NavigationContainer>
    );
  }
}
